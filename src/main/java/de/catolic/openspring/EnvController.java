package de.catolic.openspring;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by winnetou on 26.04.2017.
 */
@Controller
public class EnvController {

	@RequestMapping("/headers")
	public String showHeaders( @RequestHeader Map<String, String > requestHeaders, Model model ) {
		model.addAttribute( "headers", requestHeaders );
		return "env";
	}
}
